import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import RecipeDelete from './RecipeDelete';


const options = [
  'Delete Recipe',
  'Edit Recipe',
];

const ITEM_HEIGHT = 48;

export default function DropDownMenu({recipeDelete, id}) {

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);


  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };


  return (
    <div>
    <form>
      <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            maxHeight: ITEM_HEIGHT * 4.5,
            width: '20ch',
          },
        }}
      >
        {options.map((option) => (
          <MenuItem 
            type="submit"
            key={id} 
            selected={option === 'Delete Recipe'} 
            onClick={() => {recipeDelete(id); handleClose();}}>
            {option}
          </MenuItem>
        ))}
      </Menu>
      </form>
    </div>
  );
}