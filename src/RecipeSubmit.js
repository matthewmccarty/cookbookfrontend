import React, {useState} from 'react'
import { Card, Typography, makeStyles, TextField, Button } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';

const useStyles = makeStyles((theme) => ({
    paper: {
      textAlign: 'center',
    },
    inputField: {
     marginBottom: 10,
    }
  }));

const RecipeSubmit = ({ setErrors }) => {
    const classes = useStyles();

    const [nameText, setNameText] = useState('');
    const [descriptionText, setDescriptionText] = useState('');
    const [ingredientsText, setIngredientsText] = useState('');
    const [instructionsText, setInstructionsText] = useState('');

    const updateNameText = (event) => setNameText(event.target.value);
    const updateDescriptionText = (event) => setDescriptionText(event.target.value);
    const updateIngredientsText = (event) => setIngredientsText(event.target.value);
    const updateInstructionsText = (event) => setInstructionsText(event.target.value);


    const postData = async (event) => {

        const inputObj = {
            name: nameText,
            description: descriptionText,
            ingredients: ingredientsText,
            instructions: instructionsText,
        }

        const response = await fetch("http://localhost:8081/recipe/", {
           method: 'POST', 
           headers: {'Content-Type': 'application/json'},
           body: JSON.stringify(inputObj),
         });
         event.preventDefault();
         return response.json().then((response) => console.log(response)).catch(err => setErrors(err));
       }
    
    return (
        <Card>
            <Typography className={classes.Typography}>Submit a New Recipe</Typography>
            <form autoComplete="off">
                <TextField 
                className={classes.inputField}
                id="recipeNameInput"
                fullWidth
                multiline
                label="Name"
                variant="outlined"
                onChange={updateNameText}
                value={nameText}
                size="small">
                </TextField>

                <br/>

                <TextField 
                id="recipeDescriptionInput"
                className={classes.inputField}
                fullWidth
                multiline
                label="Description"
                variant="outlined"
                onChange={updateDescriptionText}
                value={descriptionText}
                size="small">
                </TextField>

                <br/>

                <TextField 
                id="recipeIngredientsInput"
                className={classes.inputField}
                fullWidth
                multiline
                label="ingredients"
                variant="outlined"
                onChange={updateIngredientsText}
                value={ingredientsText}
                size="small">
                </TextField>


                <TextField 
                id="recipeInstructionsInput"
                className={classes.inputField}
                fullWidth
                multiline
                label="Instructions"
                variant="outlined"
                onChange={updateInstructionsText}
                value={instructionsText}
                size="small">
                </TextField>

                <Button 
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                size="small"
                className={classes.button}
                startIcon={<SaveIcon />}
                onClick={() => postData()}
                >
                Save Recipe
                </Button>
            </form>
        </Card>
    )
}

export default RecipeSubmit