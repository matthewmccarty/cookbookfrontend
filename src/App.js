import './App.css';
import React, { useState, useEffect} from 'react'
import { Grid, makeStyles } from '@material-ui/core';
import Header from './Header';
import DataMapper from './DataMapper';
import RecipeSubmit from './RecipeSubmit';


const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(5),
    textAlign: 'center',
  },
}));

function App() {
  const classes = useStyles();

  const [recipes, setRecipes] = useState([]);
  const [hasError, setErrors] = useState(false);
 
  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const response = await fetch("http://localhost:8081/recipe/");
    return response.json().then(response => setRecipes(response)).catch(err => setErrors(err));
  }

  return (
    <div className={classes.root}>
      <Grid container  className={classes.paper} direction='column' alignContent='center'  spacing={2}>
        <Header/>
        <RecipeSubmit hasError={hasError}/>
          <Grid item>
            <DataMapper recipes={recipes} fetchData={fetchData}/>
          </Grid>
      </Grid> 
    </div>
  );
}

export default App;
