import React from 'react';

const recipeDelete = async (id) => {

    if (window.confirm("you sure?") === true) {
        const response = await fetch(`http://localhost:8081/recipe/${id}`, {method: 'POST'});
        return window.alert("deleted");
    } else {
        return null
    }
}

export default recipeDelete;