import React from 'react'
import RecipeCard from './RecipeCard';



const DataMapper = ( {recipes, fetchData} ) => recipes.map(recipe => <div key={recipe.id}><RecipeCard recipe={recipe}/></div>);
        
export default DataMapper;